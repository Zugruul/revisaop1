package CadastroFuncionarios;

import java.util.ArrayList;

public class Cadastro {
	
	private static ArrayList<Funcionario> lista = new ArrayList<>();
	
	public Cadastro() {
		
	}
	
	public static void inclui(Funcionario f) {
		if(f != null) 
			lista.add(f);
	}
	
	public static boolean exclui(String cod) {
		for(Funcionario f: lista) {
			if(f.getCodigo().compareTo(cod) == 1) {
				lista.remove(f);
				return true;
			}
		}
		return false;
	}
	
	public static String geraRelatorio(){
		String s = "";
		for(Funcionario f: lista) {
			s += f + "\n";
		}		
		return s;
	}
	
	public static boolean gravaArquivo(String nomeArquivo) {
		return false;
	}
	
	public static void ordenaNome() {
		lista.sort((f1, f2) -> f1.getNome().compareTo(f2.getNome()));
	}
	
	public static void ordenaSalario() {
		lista.sort((f1, f2) -> {if(f1.getSalarioBruto() > f2.getSalarioBruto()) return -1; else return 0;});
	}
}
