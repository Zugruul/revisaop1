package CadastroFuncionarios;

public class Vendedor extends Funcionario{
	
	private double totalVendas;
	private static int instancias = 0; // Mudan�a, contador de instancias
	
	public Vendedor(String c, String n, double s) { //Sobrecarga
		super(c, n, s);
		totalVendas = 0;
		instancias += 1;
		
	}
	
	public Vendedor(String c, String n, double s, double t) {//Sobrecarga
		super(c, n, s);
		totalVendas = t;
		instancias += 1;
	}
	
	public void setTotalVendas(double t) {
		totalVendas = t;
	}
	
	public double getTotalVendas() {
		return totalVendas;
	}
	
	@Override //Sobrescrita
	public double getSalarioLiquido() {
		double salarioBruto = (getSalarioBruto()+(getTotalVendas())*5/100);
		double inss = salarioBruto*10/100;
		double irrf;
		
		if(salarioBruto> 2500) {
			irrf = salarioBruto*7/100;
		} else {
			irrf = 0;
		}
		
		return salarioBruto - inss - irrf;
	}
	
	public static int numInstanciasCriadas() {
		return instancias;
	}
	
	@Override //Sobrescrita
	public String toString() {
		return super.toString() + " (liquido: " + getSalarioLiquido() + ") Total de Vendas: " + totalVendas;
	}
}