package CadastroFuncionarios;

public class App {
	public static void main (String args[]) {
		//Funcionario f1 = new Funcionario("001", "Fulano", 1000.00); // N�o pode ser instanciado, � abstrato
		Vendedor v1 = new Vendedor("002", "2Beltrano", 1000, 800);
		System.out.println(Vendedor.numInstanciasCriadas());
		Funcionario f2 = v1;
		Gerente g1 = new Gerente("003", "Ciclano", 2000);
		//Vendedor v2 = f2; // N�o � possivel armazenar um funcionario como um vendedor, mas � possivel o contr�rio, porque um Vendedor � um funcionario.
		
		
		
		
		Vendedor v2 = new Vendedor("002.2", "3Beltrano", 2501);
		System.out.println(Vendedor.numInstanciasCriadas());
		
		Vendedor v3 = new Vendedor("002.3", "1Beltrano", 2499);
		System.out.println(Vendedor.numInstanciasCriadas());
		
		Cadastro.inclui(v1);
		Cadastro.inclui(v2);
		Cadastro.inclui(v3);
		
		System.out.println(Cadastro.geraRelatorio());
		
		
		Cadastro.ordenaNome();
		System.out.println(Cadastro.geraRelatorio());

		
		Cadastro.ordenaSalario();
		System.out.println(Cadastro.geraRelatorio());

		}
}
 