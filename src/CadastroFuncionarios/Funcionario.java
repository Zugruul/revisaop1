package CadastroFuncionarios;

public abstract class Funcionario {
	private String codigo;
	private String nome;
	private double salario;
	
	public Funcionario(String codigo, String nome, double salario) {
		this.codigo = codigo;
		this.nome = nome;
		this.salario = salario;
	}
	
	public String getNome() {
		return nome;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public abstract double getSalarioLiquido();
	
	public double getSalarioBruto() {
		return salario;
	}
	
	public void setSalarioBruto(double salario) {
		this.salario = salario;
	}
	
	public String toString() {
		return "Funcionário (" + codigo + ") " + nome + ", Salário: " + salario;
	}
}
