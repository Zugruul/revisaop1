package CadastroFuncionarios;

public class Gerente extends Funcionario{ //N�o implementei realmente
	
	
	public Gerente(String c, String n, double s) {
		super(c, n, s);
	}

	
	@Override //Sobrescrita
	public double getSalarioLiquido() { 
		double salarioBruto = getSalarioBruto();
		double inss = salarioBruto*10/100;
		double irrf;
		
		if(salarioBruto> 2500) {
			irrf = salarioBruto*7/100;
		} else {
			irrf = 0;
		}
		
		return salarioBruto - inss - irrf;
	}
	
	@Override //Sobrescrita
	public String toString() {
		return super.toString() + " (liquido: " + getSalarioLiquido() + ") Adicional: ";
	}
}